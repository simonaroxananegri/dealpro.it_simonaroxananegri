<h1>Portale per annunci</h1>
<p>Questo progetto simula un portale che permette all'utente di creare articoli nuovi, di registrarsi/loggare, e di contattare lo staffe per informazioni con l'apposita chatbox.</p>

<a href="https://simonaroxananegri.gitlab.io/dealpro.it_simonaroxananegri/" target="a_blank">Sito Live</a>
