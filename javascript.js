document.addEventListener('scroll', () => {
    let barNav = document.querySelector("nav");
    let scrolled = window.pageYOffset;
    if (scrolled > 130) {
        barNav.classList.add('bg-white', 'bordo');
        barNav.classList.remove('bg-trasparent');
    }
    else {
        barNav.classList.add('bg-trasparent');
        barNav.classList.remove('bg-white', 'bordo');
    }
})

//primo carousel prodotti 

var swiper = new Swiper('.swiper-container', {
    slidesPerView: 1,
    centeredSlides: false,
    slidesPerGroupSkip: 1,
    grabCursor: true,
    keyboard: { enabled: true },
    breakpoints: { 769: { slidesPerView: 2, slidesPerGroup: 2 } },
    scrollbar: { el: '.swiper-scrollbar' },
    navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },
    pagination: { el: '.swiper-pagination', clickable: true }
});

//secondo carousel prodotti 

var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs
    }
});








//card articoli

const categories = ['Motori', 'Market', 'Immobili', 'Lavoro']
const categoriesWrapper = document.querySelector('#categoriesWrapper')

categories.forEach(category => {
    let div = document.createElement('div');
    div.classList.add('col-12', 'col-md-6', 'col-lg-3', 'my-3')
    div.innerHTML = `
    <img src="https://picsum.photos/1920/1082" alt="prodotto" class="img-fluid mx-auto d-block">
        <div class="text-center mb-5">
            <h2 class="mt-3">${category}</h2>
            <h3 class="small mt-3 h3">Descrizione</h3>
            <p class=" mx-auto">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iure
                quod nemo
                explicabo
                eius
                suscipit
            </p>
            <button type="button" class="btn btn-outline-danger rounded-pill button-news w-75 "> <a
                    href="articolo-singolo.html" class="text-decoration-none"> Scopri di più</a></button>
        </div>
    `
    categoriesWrapper.appendChild(div)

})